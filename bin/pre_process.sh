#!/usr/bin/env bash

OPENWRT_PATH=$1
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh $OPENWRT_PATH`

sed s/VERSION/r${REV}/g ${START}/patches/opkg.diff > ${START}/patches/opkg_with_version.diff
