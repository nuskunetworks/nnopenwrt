#!/usr/bin/env bash

OPENWRT_PATH=$1
START=$(dirname $(readlink -nf $0))/../

cd $OPENWRT_PATH
git pull
./scripts/feeds update -a
cd $START

