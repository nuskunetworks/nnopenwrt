#!/usr/bin/env bash

OPENWRT_PATH=$1
DEST=$2
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh $OPENWRT_PATH`

cd ${OPENWRT_PATH}
echo "Cleaning"
echo "    Standard Clean"
make clean
#echo "    Dir Clean"
#make dirclean

echo "    Cleaning back to scm base state"
git checkout -- ./
#git clean -df  

echo "Patching Sources"
${START}/bin/pre_process.sh $OPENWRT_PATH
${START}/bin/patch.sh $OPENWRT_PATH

echo "Building (verbose)"
make V=99 2>&1 | tee build.log

cd ${START}

#echo "Copying distribution"
#cd ${START}
#./bin/publish.sh $OPENWRT_PATH $DEST

