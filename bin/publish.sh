#!/usr/bin/env bash

OPENWRT_PATH=$1
DEST=$2
START=$(dirname $(readlink -nf $0))/../
REV=`${START}/bin/getrev.sh $OPENWRT_PATH`

${START}/bin/cpdist.sh $OPENWRT_PATH $DEST

cd ${DEST}/dist/r${REV}
gzip openwrt-x86-generic-combined-ext4.vdi
gzip openwrt-x86-generic-combined-ext4.vmdk
cd ../../

rsync --delete-before --exclude=.htaccess -OtPrc --exclude=OpenWrt-ImageBuilder* --exclude=OpenWrt-SDK* --exclude=OpenWrt-Toolchain* --exclude=packages.tar --exclude=attic dist/ mcrosson@nusku.net:/var/www/nusku.net/htdocs/openwrt/

#mount -o uid=1000 -t davfs https://www.box.com/dav /mnt/box.net
cd ${DEST}/dist/r${REV}
tar -cf ./packages.tar packages
cd ../../
#rsync --size-only --no-whole-file --inplace --delete-before --exclude=.htaccess -OPr --exclude=OpenWrt-ImageBuilder* --exclude=OpenWrt-SDK* --exclude=OpenWrt-Toolchain* --include=packages.tar --exclude=packages dist/ /mnt/box.net/openwrt/

#umount /mnt/box.net

cd $START
